#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define arraySize 5

char arrayV[arraySize];
char value;
int n = 0;
int i = 0;
int j = 0;
int position;
int found = 0;

/*************************************
*
* removeValue: Metodo para remover um valor do vetor
* params: vetor type arrayChar, value type char, n type int
*
**************************************/
int removeValue(char arrayV[], char value, int n){
	for(i = 0; i < arraySize; i++){
		if(arrayV[i] == value){
			found = 1;
			for(j = i; j < arraySize + 1; j++){
				arrayV[j] = arrayV[j + 1];
			}
		}
		
		n--;
	}
	
	if(found == 0){
		printf("\nO valor %c nao foi encontrado no vetor\n", value);
	}
}

/*************************************
*
* consultValue: Metodo para consultar um valor no vetor
* params: vetor type arrayChar, value type char, n type int
*
**************************************/
int consultValue(char arrayV[], char value, int n){
	for(i = 0; i < arraySize; i++){
		if(arrayV[i] == value){
			found = 1;
			printf("\nO valor %c esta na posicao %d do vetor", value, i + 1);
		}
	}
	
	if(found == 0){
		printf("\nO valor %c nao foi encontrado no vetor", value);
	}
}

/*************************************
*
* insertValue: Metodo para inserir um registro no vetor
* params: vetor type arrayChar, value type char, n type int
*
**************************************/
int insertValue(char arrayV[], char value, int n){
	if(arraySize == n){
		printf("Vetor esta cheio");
	}else{
		arrayV[n] = value;
	}
}


int main(){

	printf("------------------------------------------------------\n");
	printf("Exemplo 1 vetor: Renan Rosa\n");
	printf("------------------------------------------------------\n");

	
	while(n < arraySize){
		printf("\nInforme um valor para inserir no vetor: ");
		scanf(" %c", &value);
		insertValue(arrayV, value, n);
		n++;
	}
	
	printf("\nO vetor criada contem os seguintes elementos:\n\n// ");
	for(position = 0; position < arraySize; position++){
		printf("%c / ", arrayV[position]);
	}
	
	printf("\n\nInforme um valor para consultar no vetor: ");
	scanf(" %c", &value);
	consultValue(arrayV, value, n);
	
	printf("\n\nInforme um valor para remover do vetor: ");
	scanf(" %c", &value);
	removeValue(arrayV, value, n);
		
	printf("\nApos a remocao, o vetor contem os seguintes elementos:\n\n// ");
	for(position = 0; position < arraySize; position++){
		printf("%c / ", arrayV[position]);
	}
}
