#include <stdio.h>
#include <stdlib.h>


/*************************************
*
* LinkedList: definicao struct
* params: vetor type int, next type struct
*
**************************************/
typedef struct LinkedList{
   int info;
   struct LinkedList *next;
} StructList;


/*************************************
*
* startStruct: inicializacao do struct 
* params: pReceived type StructList
*
**************************************/
void startStruct(StructList **pReceived){
	(*pReceived)->next = NULL;
}


/*************************************
*
* insertStruct: Insercao de registro no struct
* params: pReceived type StructList
*
**************************************/
void insertStruct(StructList **pReceived){
	int value;
	StructList *temp;
    
    printf("\nInforme um numero para inserir no vetor: ");
    scanf(" %d", &value);
    
    temp = (StructList *) malloc(sizeof(StructList));
    temp->info = value;
    temp->next = (*pReceived)->next;
    (*pReceived)->next = temp;   
}

/*************************************
*
* searchStruct: Procura de registro no struct
* params: pReceived type StructList
*
**************************************/

void searchStruct(StructList **pReceived){
    int value;
    int achou = 0;
	StructList *temp;
    
    if((*pReceived)->next == NULL){
    	printf("\nA StructList esta vazia!");
    }else{
    	printf("\n\nInforme um numero para consultar no vetor: ");
		scanf(" %d", &value);
        
        temp = (StructList *) malloc(sizeof(StructList));
        temp = (*pReceived)->next;
        
		while(temp != NULL){
        	if(temp->info == value){
        		achou = 1;
				printf("\nO valor %d foi encontrado no vetor\n", value);
			}
            
			temp = temp->next;
        }
        
        if(achou == 0){
			printf("\nO valor %d nao foi encontrado no vetor \n", value);
		}
    }
}

/*************************************
*
* removeStruct: Remocao de registro no struct
* params: pReceived type StructList
*
**************************************/

void removeStruct(StructList **pReceived){
    int value;
    StructList *temp;
    
    if((*pReceived)->next == NULL){
    	printf("\nA StructList esta vazia!");
    }else{

        printf("\n\nInforme um numero para remover do vetor: ");
		scanf(" %d", &value);
        temp = (StructList *) malloc(sizeof(StructList));
        temp = (*pReceived)->next;

		while(temp != NULL){
        	if(temp->info == value){
				printf("\nO valor %d foi encontrado no vetor e será removido\n", value);
                temp->info = -1;
                break;
			}else{
			temp = temp->next;
            }
    }
}
}


/*************************************
*
* showStruct: mostrar struct
* params: pReceived type StructList
*
**************************************/

void showStruct(StructList **pReceived){
    StructList *temp;
    
    if((*pReceived)->next == NULL){
    	printf("\nO vetor esta vazia!");
    }else{
    	temp = (StructList *) malloc(sizeof(StructList));
        temp = (*pReceived)->next;
        
        printf("\no vetor criada contem os registros:\n\n| ");
		
		while(temp != NULL){
            if(temp->info == -1){
                printf(" | ");
            }else{
                printf("%d | ", temp->info);
            }
            temp = temp->next;
        }
    }   
}

int main(){

    printf("------------------------------------------------------\n");
	printf("Exemplo 2 vetor com structure: Renan Rosa\n");
	printf("------------------------------------------------------\n");


    StructList *structListV;
    int n = 0;
    
    void startStruct(StructList **pReceived);
    void insertStruct(StructList **pReceived);
    void searchStruct(StructList **pReceived);
    void removeStruct(StructList **pReceived);
    
    structListV = (StructList *) malloc(sizeof(struct LinkedList));
    startStruct(&structListV);
    //Populando até encher a lista  
    while(n < 5){
    	insertStruct(&structListV);
    	n++;
	}
    
    showStruct(&structListV);
	searchStruct(&structListV);
    removeStruct(&structListV);
    showStruct(&structListV);
}
